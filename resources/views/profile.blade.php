@extends('layouts.app')

@section('content')
    <section class="memberProfile" style="background-image:url({{ asset('/img/bgMember.png') }});">
        <!-- <h3 class="titleMember">My profile</h3> -->
        <div class="wrappers">
            <!-- <div class="memberContainer">
                <div class="plan">
                    <div class="item"><p class="titlePlan">BEGINNER</p></div>
                    <div class="item"><p class="pricePlan">25 €</p></div>
                    <div class="item"><p class="pariodPlan">2 ebooks per week</p></div>
                    <div class="item"><p class="workoutPlan">Workouts plan for a day</p></div>
                </div>
            </div> -->
            <div class="memberContainer">
                <form class="form" id="form" method="post" action="{{ route('user/update') }}">
                    {{ csrf_field() }}
                    <div class="inputBox">
                        <div class="wrapInput">
                            <div class="itemInput">
                                <label for="first_name" class="formLabel">First Name</label>
                                <input type="text" name="first_name" class="input" id="first_name" value="{!! Auth::user()->first_name !!}">
                                @if ($errors->has('first_name'))
                                    <p class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </p>
                                @endif
                            </div>
                            <div class="itemInput">
                                <label for="address" class="formLabel">Address</label>
                                <input type="text" name="address" class="input" id="address" value="{!! Auth::user()->address !!}">
                                @if ($errors->has('address'))
                                    <p class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </p>
                                @endif
                            </div>
                            <div class="itemInput">
                                <label for="city" class="formLabel">City</label>
                                <input type="text" name="city" class="input" id="city" value="{!! Auth::user()->city !!}">
                                @if ($errors->has('city'))
                                    <p class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </p>
                                @endif
                            </div>
                            <div class="itemInput">
                                <label for="phone" class="formLabel">Phone</label>
                                <input type="text" name="phone" class="input" id="phone" value="{!! Auth::user()->phone !!}">
                                @if ($errors->has('phone'))
                                    <p class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </p>
                                @endif
                            </div>
                            <div class="itemInput">
                                <label for="email" class="formLabel">e-mail</label>
                                <input type="text" name="email" class="input" id="email" value="{!! Auth::user()->email !!}">
                                @if ($errors->has('email'))
                                    <p class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="wrapInput">
                            <div class="itemInput">
                                <label for="last_name" class="formLabel">Last Name</label>
                                <input type="text" name="last_name" class="input" id="last_name" value="{!! Auth::user()->last_name !!}">
                                @if ($errors->has('last_name'))
                                    <p class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </p>
                                @endif
                            </div>
                            <div class="itemInput">
                                <label for="postal_code" class="formLabel">Postal code</label>
                                <input type="text" name="postal_code" class="input" id="postal_code" value="{!! Auth::user()->postal_code !!}">
                                @if ($errors->has('postal_code'))
                                    <p class="help-block">
                                        <strong>{{ $errors->first('postal_code') }}</strong>
                                    </p>
                                @endif
                            </div>
                            <div class="itemInput">
                                <label for="country" class="formLabel">Country</label>
                                <input type="text" name="country" class="input" id="country" value="{!! Auth::user()->country !!}">
                                @if ($errors->has('country'))
                                    <p class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </p>
                                @endif
                            </div>
                            <div class="radio-box">
                                <label>
                                    <input id="privacy" name="privacy" type="checkbox"/><span class="fa fa-check"></span>I certify that I am 18 years old or older and I accept your conditions. I also accept the use of cookies on this site.
                                </label>
                            </div>
                        </div>

                    </div>
                    <input type="submit" class="btnSubmit" value="Save" disabled>
                </form>

            </div>
        </div>
    </section>

    <div class="overlayThankYou js-overlay">
        <div class="popup js-popup-campaign">
            <div class="btnClosse"></div>
            <div class="titleWrap">
                <!-- <h5 class="title">Member Area</h5> -->
                <p class="textMember">Thank you for sending your information
            </div>
            <div class="button-stage">
                <!--OK-->
                <div class="parlor-widget__bubble">
                    <!-- BUTTON VARIABLES -->
                    <div class="parlor-button-wrapper">
                        <div class="parlor-button">
                            <!-- A-OK -->
                            <svg width="22px" height="30px" viewBox="0 0 22 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="parlor-hand-ok">
                                <path d="M17.3825517,16.3008093 C15.1643831,22.9284305 15.076861,27.0209926 15.0734077,27.3336341 L15.0847051,27.3529412 C15.0847051,27.3529412 14.9785639,29.3817829 10.9774237,29.3817829 C6.97628353,29.3817829 7.05882353,27.3529412 7.05882353,27.3529412 C7.05882353,25.0359583 5.8182389,22.4915608 3.52941176,21.1764706 L3.6388172,21.1764706 C1.4687429,19.972298 0,17.6576706 0,15 C0,11.1015194 3.16034294,7.94117647 7.05882353,7.94117647 C9.16710446,7.94117647 11.0595098,8.8654498 12.3529412,10.3308981 L12.3529412,1.32639759 C12.3529412,0.593848428 12.9404093,-8.8817842e-16 13.6764706,-8.8817842e-16 C14.4074357,-8.8817842e-16 15,0.587149227 15,1.32639759 L15,8.68435206 L17.0180246,2.47351094 C17.2452058,1.77431909 17.988088,1.38904976 18.6881238,1.6165052 C19.383313,1.84238584 19.7651676,2.58473642 19.5355272,3.2914971 L17.5327428,9.45543338 L18.7827228,6.64793242 C19.0786648,5.98323593 19.855252,5.68333778 20.5276774,5.98272087 C21.1954472,6.28003116 21.4985228,7.05618692 21.2009314,7.72458824 L17.3825517,16.3008093 Z M7.05882353,18.9215686 C9.22464608,18.9215686 10.9803922,17.1658225 10.9803922,15 C10.9803922,12.8341775 9.22464608,11.0784314 7.05882353,11.0784314 C4.89300098,11.0784314 3.1372549,12.8341775 3.1372549,15 C3.1372549,17.1658225 4.89300098,18.9215686 7.05882353,18.9215686 Z" id="parlor-hand-ok"></path>
                            </svg>
                        </div>
                    </div>
                    <!-- END PARLOR BUTTON -->
                </div>
            </div>
        </div>
    </div>
@endsection