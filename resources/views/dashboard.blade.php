@extends('layouts.app')

@section('content')

    <section class="memberDashboard" style="background-image:url({{ asset('/img/bgMember.png') }});">
            <!-- <h3 class="titleMember">My profile</h3> -->
            <div class="wrappers">
                <div class="dashContainer">
                    <div class="itemDash">
                        <img src="img/bookIcon.png" alt="">
                    </div>
                    <div class="itemDash">
                        <h3 class="title">E-book</h3>
                        <p class="subTitle">Available / Remaining</p>
                        <p class="counter">29/296</p>
                    </div>
                </div>
                <div class="dashContainer">
                    <div class="itemDash">
                        <img src="img/clockIcon.png" alt="">
                    </div>
                    <div class="itemDash">
                        <h3 class="title">Expiration date</h3>
                        <p class="subTitle">Available / Remaining</p>
                        <p class="counter">29/296</p>
                    </div>
                </div>
            </div>
        </section>

@endsection