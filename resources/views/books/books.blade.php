@extends('layouts.app')

@section('content')
<section class="memberEbooks" style="background-image:url({{ asset('/img/bgMember.png') }});">
    <!-- <h3 class="titleMember">My profile</h3> -->
    <div class="wrappers">
        <div class="ebooksContainer">
            <div class="itemEbooks">
                <div class="imgEbooks">
                    <img src="img/book1.png" alt="">
                </div>
                <h3 class="title">50 Things To Do Before You Deliver</h3>
                <p class="subTitle">Jill Krause</p>
                <p class="">
                    <a class="btn-download" href="files/Зандстра М. - PHP. Объекты, шаблоны.pdf">
                        download
                    </a>
                </p>
            </div>
            <div class="itemEbooks">
                <div class="imgEbooks">
                    <img src="img/book2.png" alt="">
                </div>
                <h3 class="title">The Impatient Woman's Guide to Getting Pregnant</h3>
                <p class="subTitle">Jean M. Twenge PhD</p>
                <p class="">
                    <a class="btn-download" href="files/Вайсфельд М. - Объектно-ориентированное мышление.pdf">
                        download
                    </a>
                </p>
            </div>
            <div class="itemEbooks">
                <div class="imgEbooks">
                    <img src="img/book3.png" alt="">
                </div>
                <h3 class="title">Mom Life: A Snarky Adult Coloring Book</h3>
                <p class="subTitle">Papeterie Bleu</p>
                <p class="">
                    <a class="btn-download" href="files/UseMind.org_PHP_Obyekty_shablony_i_metodiki_programmirovaniya__.pdf">
                        download
                    </a>
                </p>
            </div>
            <div class="itemEbooks">
                <div class="imgEbooks">
                    <img src="img/book1.png" alt="">
                </div>
                <h3 class="title">50 Things To Do Before You Deliver</h3>
                <p class="subTitle">Jill Krause</p>
                <p class="">
                    <a class="btn-download" href="files/Зандстра М. - PHP. Объекты, шаблоны.pdf">
                        download
                    </a>
                </p>
            </div>
            <div class="itemEbooks">
                <div class="imgEbooks">
                    <img src="img/book2.png" alt="">
                </div>
                <h3 class="title">The Impatient Woman's Guide to Getting Pregnant</h3>
                <p class="subTitle">Jean M. Twenge PhD</p>
                <p class="">
                    <a class="btn-download" href="files/Зандстра М. - PHP. Объекты, шаблоны.pdf">
                        download
                    </a>
                </p>
            </div>
            <div class="itemEbooks">
                <div class="imgEbooks">
                    <img src="img/book3.png" alt="">
                </div>
                <h3 class="title">Mom Life: A Snarky Adult Coloring Book</h3>
                <p class="subTitle">Papeterie Bleu</p>
                <p class="">
                    <a class="btn-download" href="files/Зандстра М. - PHP. Объекты, шаблоны.pdf">
                        download
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>
@endsection