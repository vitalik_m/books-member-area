@extends('layouts.app')

@section('content')
    <section class="memberManage" style="background-image:url({{ asset('/img/bgMember.png') }});">
        <div class="wrappers">
            <div class="wrapContainer">
                <div class="manageContainer">
                    <div class="itemManage">
                        <h3 class="titleManage">package expiration date</h3>
                        <div class="containerInfo">
                            <p class="info">from <span class="date"> 20.01.2019</span> to <span class="date">20.02.2019</span> </p>
                        </div>
                    </div>
                </div>
                <div class="manageContainer">
                    <div class="itemManage">
                        <h3 class="titleManage">available books</h3>
                        <div class="containerInfo">
                            <p class="infoAvailable">3</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btnItem">
                <a href="#">Sign out</a>
            </div>
        </div>
    </section>
@endsection